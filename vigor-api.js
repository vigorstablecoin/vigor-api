
const eosjs = require('./eosjs')
var express = require("express");
var app = express();

async function getquery(){
    const query = {code:"vigorlending",scope:"vigorlending",limit:100,table:'globalstats'}
    const pairData = (await eosjs().rpc.get_table_rows(query)).rows[0]
    return pairData
}

app.get("/get_info", async(req, res, next) => {
    var pairData = await getquery()
    const tvl = parseFloat(pairData.valueofcol) + parseFloat(pairData.valueofins) + parseFloat(pairData.savings) + parseFloat(pairData.l_totaldebt)
    pairData["TVL"] = tvl;
    res.json(pairData);
});

app.listen(8081, () => {
 console.log("Server running on port 8081");
});