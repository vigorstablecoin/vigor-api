const { Api, JsonRpc } = require('eosjs')
var url = require('url')
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');
const fetch = require('node-fetch')
const { TextEncoder, TextDecoder } = require('util')
const env = require('./.env.js')

const tapos = {
  blocksBehind: 3,
  expireSeconds: 90
}

function init(){
  //console.log('eosjs',global.accountName)
  const endpoint = 'https://eos.vigor.ai'
  var signatureProvider
  if (env.keys[global.accountName]) signatureProvider = new JsSignatureProvider([env.keys[global.accountName]])
  else signatureProvider = new JsSignatureProvider([])
  const rpc = new JsonRpc(endpoint, { fetch })
  const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() })
  const eosjs = { rpc, api, doActions }

  async function doActions(actions){
    try {
      const result = await api.transact({actions},tapos)
      if (!result) throw("There was an error")
      console.log(result.transaction_id)
      return result
    } catch (error) {
      console.error(error.toString())
    }
  }

  return eosjs
}

module.exports = init
